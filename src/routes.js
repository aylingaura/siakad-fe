import React from 'react';

const AdminDashboard = React.lazy(() => import('./views/pages/admin/Dashboard'));
const UserManagement = React.lazy(() => import('./views/pages/admin/userManagement/UserManagement'));

const DeveloperDashboard = React.lazy(() => import('./views/pages/developer/Dashboard'));

const routes = [
  { path: '/', exact: true, name: 'Home' },

  { path: '/admin/dashboard', name: 'Dashboard Admin', component: AdminDashboard },
  { path: '/user-management/user', name: 'User Management / User', component: UserManagement },

  { path: '/developer/dashboard', name: 'Dashboard Developer', component: DeveloperDashboard },
  { path: '/developer/project', name: 'Project', component: DeveloperDashboard },
  { path: '/developer/task', name: 'Task', component: DeveloperDashboard },

];

export default routes;
