const _nav = [

  {
    _tag: 'CSidebarNavItem',
    name: 'Dashboard',
    to: '/admin/dashboard',
    icon: 'cilCircle'
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Project',
    to: '/admin/project',
    icon: 'cilCircle'
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Task',
    to: '/admin/task',
    icon: 'cilCircle'
  },

]

export default _nav
