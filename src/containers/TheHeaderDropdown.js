import React, { useState } from 'react'
import { useContext } from '../components/Context'
import Loader from '../components/Loader_'
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
} from '@coreui/react'
import {
  BiGroup,
  BiLinkExternal,
  BiHappyBeaming,
  BiDetail,
  BiCog
} from "react-icons/bi"
import axios from 'axios'

const TheHeaderDropdown = (props) => {

  const [loader, setLoader] = useState(false)
  const { logOut } = useContext()

  const logout = () => {

    axios.interceptors.request.use(config => {
      setLoader(true)
      return config;
    }, error => {
      return Promise.reject(error);
    });

    logOut()

  }

  return (
    <>
      <Loader isActive={loader} />

      <CDropdown inNav className="c-header-nav-items mx-2" direction="down">

        <CDropdownToggle className="c-header-nav-link" caret={false}>
          <div className="c-avatar">
            <BiGroup className="text-dark-50" style={{ fontSize: 30 }} />
          </div>
        </CDropdownToggle>

        <CDropdownMenu className="pt-0" placement="bottom-end">

          <CDropdownItem header tag="div" color="light" className="text-center">
            <div className="row">
              <div className="col-md-12 text-center">
                <span className="font-weight-bold">{JSON.parse(localStorage.getItem('user')) ? JSON.parse(localStorage.getItem('user')).name : '-' }</span>
              </div>
              <div className="col-md-12 text-center">
                <small>( {JSON.parse(localStorage.getItem('user')) ? JSON.parse(localStorage.getItem('user')).user_type.name : '-' } )</small>
              </div>
            </div>
          </CDropdownItem>
          <CDropdownItem>
            <BiHappyBeaming className="mfe-2" />Profile
          </CDropdownItem>
          <CDropdownItem>
            <BiCog className="mfe-2" />Setting
          </CDropdownItem>
          <CDropdownItem>
            <BiDetail className="mfe-2" />About
          </CDropdownItem>
          <hr />
          <CDropdownItem onClick={logout}>
            <BiLinkExternal className="mfe-2" />Log Out
          </CDropdownItem>

        </CDropdownMenu>

      </CDropdown>
    </>
  )

}

export default TheHeaderDropdown;
