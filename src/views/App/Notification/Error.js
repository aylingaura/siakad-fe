import React from 'react'
import { CAlert } from '@coreui/react'
import { FaUndoAlt } from 'react-icons/fa'

const Error = (props) => {

  if(props.message){
    return (
      <CAlert color="danger">
        <div className="row">
          <div className="col-md-6">
            <strong>Error Message : </strong><br/> {props.message}
          </div>
          <div className="col-md-6 text-right">
            <button type="button" name="" id="" className="btn btn-sm btn-outline" onClick={() => { window.location.reload() }}><FaUndoAlt/></button>
          </div>
        </div>
      </CAlert>
    )
  }
  else{
    return (
      <></>
    )
  }
}

export default Error
