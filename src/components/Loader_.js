import React, { Component } from 'react'
import { CSpinner } from '@coreui/react'

export default class Loader extends Component {
    render() {
        return (
            <div style={{ display: this.props.isActive ? "block" : "none", backgroundColor: "rgba(44, 62, 80, 0.5)", height: "100%", width: "100%", position: "fixed", zIndex: "999999", top: "0", left: "0", right: "0", bottom: "0" }}>
                <div className="d-flex justify-content-between align-items-center" style={{ paddingLeft: "50%", paddingTop: "20%" }}>
                    <CSpinner color="secondary" style={{ width: '4rem', height: '4rem' }} />
                </div>
            </div>
        )
    }
}
