import create from 'zustand'
import axios from 'axios'
import { Redirect } from 'react-router-dom'

export const useContext = create((set, get) => ({

    // examples
    test: 0,
    testFunction: () =>
        set(state => ({
            test: state.test + 1
            })
        ),


    // check expaired session login based on database
    checkLogin: () => {
        var today = new Date();
        var expired_token_date = new Date(localStorage.getItem("expired_token_date"));

        if(today.getTime() > expired_token_date.getTime()){
            alert('Session Expired, Please Login Again')
            get().logOut()
        }
    },

    // logout app
    logOut: () => {
        const url = process.env.REACT_APP_ROOT_URL + 'logout'
        const config = {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem("access_token")
            }
        }

        axios.get(url, config)
            .then(res => {
                localStorage.removeItem("access_token")
                localStorage.removeItem("user")
                localStorage.removeItem("expired_token_date")
                localStorage.removeItem("id_user_type")
                window.location.reload()
            })
            .catch(error => {
                console.log('error')
            })
    },

    // header 
    headers: {
        'Authorization': 'Bearer ' + localStorage.getItem("access_token")
    },

    formatRupiah: (num) => {
        return 'Rp. ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    },

}))
